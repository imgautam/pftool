<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    /**
     * Get permission details belongs to current role
     */   
    public function permissions()
    {
        return $this->hasMany('App\Models\Permission');
    }
}
