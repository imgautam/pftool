<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'module', 'role_id', 'read', 'write', 'delete'
    ];

    /**
     * Get role details belongs to current permission
     */   
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
}
