<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'role_id'
    ];

    /**
     * Get user details belongs to current permission
     */   
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get role details belongs to current permission
     */   
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
}
