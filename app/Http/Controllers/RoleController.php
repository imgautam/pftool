<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Repositories\RoleRepository;

class RoleController extends BaseController
{
    /**
     * The role repository implementation.
     *
     * @var RoleRepository
     */
    protected $repository;

    /**
     * Create a new controller instance.
     *
     * @param  RoleRepository $repository
     * @return void
     */
    public function __construct(RoleRepository $repository)
    {
        $this->middleware('checkPermission:Role-read', ['only' => ['index', 'show']]);
        $this->middleware('checkPermission:Role-write', ['only' => ['create', 'store', 'edit', 'update']]);
        $this->middleware('checkPermission:Role-delete', ['only' => ['destroy']]);

        $this->repository = $repository;

        view()->share('currentPermissions', session('permissions'));
        view()->share('user', session('user'));
    }

    /**
     * Render view to list roles
     */
    function index() 
    {
        $roles = $this->repository->records();
        return view('admin.role.roles')->withRoles($roles);
    }

    /**
     * Render view to show create role form
     */
    function create() 
    {
        return view('admin.role.create');
    }

    /**
     * Store newly created role
     * 
     * @param Request $request
     */
    function store(Request $request) 
    {
        $data = ['title' => $request->get('title')];
        $this->repository->store($data, $data);

        return redirect('/roles');
    }

    /**
     * Render view to show edit role form
     */
    function edit($id) 
    {
        $role = $this->repository->find($id);
        return view('admin.role.edit')->withRole($role);
    }

    /**
     * Update role
     */
    function update(Request $request, $id) 
    {
        $this->repository->update($request, $id);
        return redirect('/roles');
    }

    /**
     * Delete role
     */
    function destroy($id) 
    {
        $this->repository->remove($id);
        return redirect('/roles');
    }
}
