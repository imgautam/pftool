<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Repositories\UserRepository;
use App\Services\PermissionService;

class AuthController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $url = 'https://authdev.peoplefone.com/oauth/authorize?redirect_uri=http://localhost:9090/auth&response_type=code&scope=&locale=en_CH&client_id=11';

    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $repository;

    /**
     * The permission service implementation.
     *
     * @var PermissionService
     */
    protected $permissionService;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository $repository
     * @return void
     */
    public function __construct(UserRepository $repository, PermissionService $permissionService)
    {
        $this->repository = $repository;
        $this->permissionService = $permissionService;
    }
    
    function index() 
    {
        return redirect($this->url);        
    }

    function authenticated(Request $request) 
    {
        $code = $request->get('code');

        $http = new Client();       

        $response = $http->post('https://authdev.peoplefone.com/oauth/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => 11,
                'client_secret' => '7MZeB9lH9iN9mB8eKCU7NCmMFMFFZTqHJpfrhDVK',
                'redirect_uri' =>  'http://localhost:9090/auth',
                'code' => $code                
            ],
        ])->getBody();

        $accessToken = json_decode($response)->access_token;
        
        if($accessToken)
        {
            $tokenArray = explode('.', $accessToken);         
            $payload = json_decode(base64_decode($tokenArray[1]));

            $this->register($payload->email, $payload->sub);
        }

        return redirect('/admin');
    }

    function register($email, $uniqueId) {
        if($uniqueId)
        {                                    
        	$dataToCheck = ['unique_id' => $uniqueId];
        	$data = ['email' => $email, 'unique_id' => $uniqueId];

            $user = $this->repository->store($dataToCheck, $data);

            $user = $this->repository->find($user->id);
            $permissions = $this->permissionService->prepareData($user->userRoles);

            session(['user' => $user, 'permissions' => $permissions]);
        }
    }
}
