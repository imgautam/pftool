<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Repositories\UserRepository;

class UserController extends BaseController
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $repository;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository $repository
     * @return void
     */
    public function __construct(UserRepository $repository)
    {
        $this->middleware('checkPermission:User-read', ['only' => ['index', 'show']]);

        $this->repository = $repository;

        view()->share('currentPermissions', session('permissions'));
        view()->share('user', session('user'));
    }

    function index() 
    {
        $users = $this->repository->records();
        return view('admin.user.users')->withUsers($users);
    }
}
