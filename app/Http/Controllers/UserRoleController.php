<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Repositories\UserRoleRepository;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;

class UserRoleController extends BaseController
{
    /**
     * The userRole repository implementation.
     *
     * @var UserRoleRepository
     */
    protected $repository;

    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * The role repository implementation.
     *
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * Create a new controller instance.
     *
     * @param  UserRoleRepository $repository
     * @return void
     */
    public function __construct(UserRoleRepository $repository, UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->middleware('checkPermission:User_Role-read', ['only' => ['index', 'show']]);
        $this->middleware('checkPermission:User_Role-write', ['only' => ['create', 'store', 'edit', 'update']]);
        $this->middleware('checkPermission:User_Role-delete', ['only' => ['destroy']]);

        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;

        view()->share('currentPermissions', session('permissions'));
        view()->share('user', session('user'));
    }

    /**
     * Render view to list userRoles
     */
    function index() 
    {
        $users = $this->userRepository->records();
        return view('admin.user-role.user-roles')->withUsers($users);
    }

    /**
     * Render view to show create userRole form
     */
    function create() 
    {
        $users = $this->userRepository->records();
        $roles = $this->roleRepository->records();

        return view('admin.user-role.create')->withUsers($users)->withRoles($roles);
    }

    /**
     * Store newly created userRole
     * 
     * @param Request $request
     */
    function store(Request $request) 
    {
        $userId = $request->get('user_id');
        $roleIds = $request->get('role_ids');

        foreach($roleIds as $roleId) {            
            $data = ['user_id' => $userId, 'role_id' => $roleId];
            $this->repository->store($data);
        }

        return redirect('/user-roles');
    }

    /**
     * Render view to show edit userRole form
     */
    function edit($id) 
    {
        $userRole = $this->repository->findByUserId($id);
        $user = $this->userRepository->find($id);
        $roles = $this->roleRepository->records();

        return view('admin.user-role.edit')->withUserRoles($userRole)->withUser($user)->withRoles($roles);
    }

    /**
     * Update userRole
     */
    function update(Request $request, $id) 
    {
        $roleIds = $request->get('role_ids');

        $this->repository->removeByUserId($id);
        
        foreach($roleIds as $roleId) {            
            $data = ['user_id' => $id, 'role_id' => $roleId];
            $this->repository->store($data);
        }

        return redirect('/user-roles');
    }

    /**
     * Delete userRole
     */
    function destroy($id) 
    {
        $this->repository->removeByUserId($id);
        return redirect('/user-roles');
    }
}
