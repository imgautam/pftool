<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Repositories\UserRepository;

class AdminController extends BaseController
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $repository;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository $repository
     * @param  PermissionService $repository
     * @return void
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;

        view()->share('currentPermissions', session('permissions'));
        view()->share('user', session('user'));
    }
    
    function index() 
    {
        return view('admin.layout');
    }
}
