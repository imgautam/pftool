<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;

class PermissionController extends BaseController
{
    /**
     * The permission repository implementation.
     *
     * @var PermissionRepository
     */
    protected $repository;

    /**
     * The role repository implementation.
     *
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * Create a new controller instance.
     *
     * @param  PermissionRepository $repository
     * @param  RoleRepository $roleRepository
     * @return void
     */
    public function __construct(PermissionRepository $repository, RoleRepository $roleRepository)
    {
        $this->middleware('checkPermission:Permission-read', ['only' => ['index', 'show']]);
        $this->middleware('checkPermission:Permission-write', ['only' => ['create', 'store', 'edit', 'update']]);
        $this->middleware('checkPermission:Permission-delete', ['only' => ['destroy']]);

        $this->repository = $repository;
        $this->roleRepository = $roleRepository;       

        view()->share('currentPermissions', session('permissions'));
        view()->share('user', session('user'));
    }

    /**
     * Render view to list permissions
     */
    function index() 
    {
        $permissions = $this->repository->records();
        return view('admin.permission.permissions')->withPermissions($permissions);
    }

    /**
     * Render view to show create permission form
     */
    function create() 
    {
        $roles = $this->roleRepository->records();
        return view('admin.permission.create')->withRoles($roles);
    }

    /**
     * Store newly created permission
     * 
     * @param Request $request
     */
    function store(Request $request) 
    {
        $data = $request->all();
        $dataToCheck = [
            'role_id' => $data['role_id'],
            'module' => $data['module'],
        ];

        if(!isset($data['read']))
            $data['read'] = 0;
        if(!isset($data['write']))
            $data['write'] = 0;
        if(!isset($data['delete']))
            $data['delete'] = 0;

        $this->repository->store($dataToCheck, $data);

        return redirect('/permissions');
    }

    /**
     * Render view to show edit permission form
     */
    function edit($id) 
    {
        $permission = $this->repository->find($id);

        $roles = $this->roleRepository->records();

        return view('admin.permission.edit')->withPermission($permission)->withRoles($roles);
    }

    /**
     * Update permission
     */
    function update(Request $request, $id) 
    {
        $data = $request->all();
        
        if(!isset($data['read']))
            $data['read'] = 0;
        if(!isset($data['write']))
            $data['write'] = 0;
        if(!isset($data['delete']))
            $data['delete'] = 0;

        $this->repository->update($data, $id);
        return redirect('/permissions');
    }

    /**
     * Delete permission
     */
    function destroy($id) 
    {
        $this->repository->remove($id);
        return redirect('/permissions');
    }
}
