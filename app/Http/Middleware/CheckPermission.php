<?php

namespace App\Http\Middleware;

use Closure;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module)
    {
        $user = session('user');
        $permissions = session('permissions');

        if($user->type == 'admin')
            return $next($request);
        
        $modulePermission = explode("-", $module);
        $moduleName = str_replace("_"," ", $modulePermission[0]);;
        $permissionName = $modulePermission[1];

        try{
            if($permissions && $permissions[$moduleName][$permissionName]) 
                return $next($request);
            return redirect('/');           
        } catch (JWTException $e) {
            return redirect('/');
        }
    }
}
