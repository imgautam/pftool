<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    /**
     * Saves a user in database.
     *
     * @param Array $dataToCheck
     * @param Array $data
     *
     * @return User
     */
    public function store($dataToCheck, $data)
    {
        return User::updateOrCreate($dataToCheck, $data);
    }

    /**
     * Get list of users in database.
     *
     * @param Array $filter
     * 
     * @return User[]
     */
    public function records()
    {
        return User::where('type', 'user')->with('userRoles')->get();
    }

    /**
     * Get user by uniqueId
     *
     * @param string $uniqueId
     * 
     * @return User
     */
    public function getByUniqueId($uniqueId)
    {
        return User::where('unique_id', $uniqueId)->first();
    }

    /**
     * Get User from database.
     *
     * @param Integer $id
     * 
     * @return User
     */
    public function find($id)
    {
        return User::where('id', $id)->with('userRoles.role.permissions')->first();
    }

    /**
     * Updates a user in database.
     *
     * @param User $user
     * 
     * @return boolean
     */
    public function update($user)
    {
        return $user->update();
    }
}
