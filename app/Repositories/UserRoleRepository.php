<?php

namespace App\Repositories;

use Illuminate\Http\Request;

use App\Models\UserRole;

class UserRoleRepository
{
    /**
     * Saves a userRole in database.
     *
     * @param Array $dataToCheck
     * @param Array $data
     *
     * @return UserRole
     */
    public function store($data)
    {
        $userRole = new UserRole($data);
        return $userRole->save();
    }

    /**
     * Get list of userRoles in database.
     *
     * @param Array $filter
     * 
     * @return UserRole[]
     */
    public function records()
    {
        return UserRole::all();
    }

    /**
     * Get UserRole from database.
     *
     * @param Integer $id
     * 
     * @return UserRole
     */
    public function find($id)
    {
        return UserRole::find($id);
    }

    /**
     * Get UserRole from database.
     *
     * @param Integer $id
     * 
     * @return UserRole
     */
    public function findByUserId($id)
    {
        return UserRole::where('user_id', $id)->get();
    }

    /**
     * Updates a userRole in database.
     *
     * @param Request $request
     * @param Integer $id
     * 
     * @return boolean
     */
    public function update($data, $id)
    {
        $userRole = UserRole::find($id);
        return $userRole->update($data);
    }

    /**
     * Delete a userRole in database.
     *
     * @param Integer $id
     * 
     * @return boolean
     */
    public function removeByUserId($id)
    {
        return UserRole::where('user_id', $id)->forceDelete();
    }
}
