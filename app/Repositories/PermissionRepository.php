<?php

namespace App\Repositories;

use Illuminate\Http\Request;

use App\Models\Permission;

class PermissionRepository
{
    /**
     * Saves a permission in database.
     *
     * @param Array $dataToCheck
     * @param Array $data
     *
     * @return Permission
     */
    public function store($dataToCheck, $data)
    {
        return Permission::updateOrCreate($dataToCheck, $data);
    }

    /**
     * Get list of permissions in database.
     *
     * @param Array $filter
     * 
     * @return Permission[]
     */
    public function records()
    {
        return Permission::whereHas('role')->with('role')->get();
    }

    /**
     * Get Permission from database.
     *
     * @param Integer $id
     * 
     * @return Permission
     */
    public function find($id)
    {
        return Permission::find($id);
    }

    /**
     * Updates a permission in database.
     *
     * @param Request $request
     * @param Integer $id
     * 
     * @return boolean
     */
    public function update($data, $id)
    {
        $permission = Permission::find($id);
        return $permission->update($data);
    }

    /**
     * Delete a permission in database.
     *
     * @param Integer $id
     * 
     * @return boolean
     */
    public function remove($id)
    {
        return Permission::find($id)->forceDelete();
    }
}
