<?php

namespace App\Repositories;

use Illuminate\Http\Request;

use App\Models\Role;

class RoleRepository
{
    /**
     * Saves a role in database.
     *
     * @param Array $dataToCheck
     * @param Array $data
     *
     * @return Role
     */
    public function store($dataToCheck, $data)
    {
        return Role::updateOrCreate($dataToCheck, $data);
    }

    /**
     * Get list of roles in database.
     *
     * @param Array $filter
     * 
     * @return Role[]
     */
    public function records()
    {
        return Role::all();
    }

    /**
     * Get Role from database.
     *
     * @param Integer $id
     * 
     * @return Role
     */
    public function find($id)
    {
        return Role::find($id);
    }

    /**
     * Updates a role in database.
     *
     * @param Request $request
     * @param Integer $id
     * 
     * @return boolean
     */
    public function update(Request $request, $id)
    {
        $role = Role::find($id);
        $role->title = $request->get('title');

        return $role->update();
    }

    /**
     * Delete a role in database.
     *
     * @param Integer $id
     * 
     * @return boolean
     */
    public function remove($id)
    {
        return Role::find($id)->forceDelete();
    }
}
