<?php

namespace App\Services;

class PermissionService
{
    /**
     * Prepare data for module access rolewise
     * 
     * @param [] $userRoles
     * @return []
     */
    public function prepareData($userRoles) {
        $permissions = [];
        foreach($userRoles as $userRole) {
            foreach($userRole->role->permissions as $permission) {
                if(!isset($permissions[$permission->module])) {
                    $permissions[$permission->module] = [];
                    $permissions[$permission->module]['read'] = 0;
                    $permissions[$permission->module]['write'] = 0;
                    $permissions[$permission->module]['delete'] = 0;
                }
                
                if(!$permissions[$permission->module]['read'])
                    $permissions[$permission->module]['read'] = $permission->read;
                if(!$permissions[$permission->module]['write'])
                    $permissions[$permission->module]['write'] = $permission->write;
                if(!$permissions[$permission->module]['delete'])
                    $permissions[$permission->module]['delete'] = $permission->delete;
            }
        }

        return $permissions;
    }
}
