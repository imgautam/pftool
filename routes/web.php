<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/authenticate', 'AuthController@index');
Route::get('/auth', 'AuthController@authenticated');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index');
});

Route::group(['prefix' => 'users'], function () {
    Route::get('/', 'UserController@index');
});

Route::resource('roles', 'RoleController');
Route::resource('permissions', 'PermissionController');
Route::resource('user-roles', 'UserRoleController');