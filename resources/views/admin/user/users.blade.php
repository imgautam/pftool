@extends('admin.layout')

@section('content')
    <div class="col-lg-12 col-md-12">
        <h3 class="content-title">Users</h3>
    </div>
    <div class="col-lg-12 col-md-12">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Email</th>
                    <th scope="col">Unique Id</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                @foreach($users as $user)
                    <tr>
                        <th scope="row">{{$i++}}</th>
                        <td>{{$user->email}}</td>
                        <td>{{$user->unique_id}}</td>
                    </tr>            
                @endforeach
            </tbody>
        </table>
    </div>
@endsection