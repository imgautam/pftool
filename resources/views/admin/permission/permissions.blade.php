@extends('admin.layout')

@section('content')
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <h3 class="content-title">Permissions</h3>
            </div>
            @if('admin' == $user->type || $currentPermissions['Permission']['write'])
                <div class="col-lg-4 col-md-4">
                    <a href="permissions/create" class="pull-right">Add New</a>
                </div>
            @endif
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Module</th>
                    <th scope="col">Role</th>
                    <th scope="col">Permission</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                @foreach($permissions as $permission)
                    <tr>
                        <th scope="row">{{$i++}}</th>
                        <td>{{$permission->module}}</td>
                        <td>{{$permission->role->title}}</td>
                        <td>
                            @if($permission->read)
                                Read
                            @endif
                            @if($permission->write)
                                Write
                            @endif
                            @if($permission->delete)
                                Delete
                            @endif
                        </td>
                        <td>    
                            @if('admin' == $user->type || $currentPermissions['Permission']['write'])
                                <a href="/permissions/{{$permission->id}}/edit">Edit</a>
                            @endif
                            @if('admin' == $user->type || $currentPermissions['Permission']['delete'])
                                <form action="/permissions/{{$permission->id}}" method="post">
                                    @method('DELETE')
                                    @csrf()
                                    <button type="submit" class="btn btn-link" style="color:red">Remove</button>
                                </form>
                            @endif
                        </td>
                    </tr>            
                @endforeach
            </tbody>
        </table>
    </div>
@endsection