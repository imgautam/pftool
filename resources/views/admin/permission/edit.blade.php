@extends('admin.layout')

@section('content')
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <h3 class="content-title">Edit Permission</h3>
            </div>
            <div class="col-lg-4 col-md-4">
                <a href="/permissions" class="pull-right">View Permissions</a>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <form action="/permissions/{{$permission->id}}" method="post">
            @method('PUT')
            @csrf()
            <div class="form-group">
                <label for="roleId">Select Role</label>
                <select class="form-control" id="roleId" name="role_id" required>
                    @foreach($roles as $role)
                        @if($role->id == $permission->role_id)
                            <option value="{{$role->id}}" selected>{{$role->title}}</option>
                        @else
                            <option value="{{$role->id}}">{{$role->title}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="module">Select Module</label>
                <select class="form-control" id="module" name="module" required>
                    <option value="{{$permission->module}}" hidden>{{$permission->module}}</option>
                    <option value="User">User</option>
                    <option value="Role">Role</option>
                    <option value="Permission" >Permission</option>
                    <option value="User Role" >User Role</option>
                </select>
            </div>
            <div class="form-group">                
                <label class="form-check-label col-lg-2 col-md-3" for="read">
                    @if($permission->read)
                        <input type="checkbox" id="read" class="form-check-input" name="read" value="1" checked> Read
                    @else
                        <input type="checkbox" id="read" class="form-check-input" name="read" value="1"> Read
                    @endif
                </label>
                <label class="form-check-label col-lg-2 col-md-3" for="write">
                    @if($permission->write)
                        <input type="checkbox" class="form-check-input" id="write" name="write" value="1" checked> Write
                    @else
                        <input type="checkbox" class="form-check-input" id="write" name="write" value="1"> Write
                    @endif
                </label>
                <label class="form-check-label col-lg-2 col-md-3" for="delete">
                    @if($permission->delete)
                        <input type="checkbox" class="form-check-input" id="delete" name="delete" value="1" checked> Delete
                    @else
                        <input type="checkbox" class="form-check-input" id="delete" name="delete" value="1"> Delete 
                    @endif
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection