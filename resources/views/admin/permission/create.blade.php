@extends('admin.layout')

@section('content')
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <h3 class="content-title">Create Permission</h3>
            </div>
            <div class="col-lg-4 col-md-4">
                <a href="/permissions" class="pull-right">View Permissions</a>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <form action="/permissions" method="post">
            @csrf()
            <div class="form-group">
                <label for="roleId">Select Role</label>
                <select class="form-control" id="roleId" name="role_id" required>
                    <option value="" hidden>Select Role</option>
                    @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="module">Select Module</label>
                <select class="form-control" id="module" name="module" required>
                    <option value="" hidden>Select Module</option>
                    <option value="User">User</option>
                    <option value="Role">Role</option>
                    <option value="Permission" >Permission</option>
                    <option value="User Role" >User Role</option>
                </select>
            </div>
            <div class="form-group">                
                <label class="form-check-label col-lg-2 col-md-3" for="read">
                    <input type="checkbox" id="read" class="form-check-input" name="read" value="1"> Read
                </label>
                <label class="form-check-label col-lg-2 col-md-3" for="write">
                    <input type="checkbox" class="form-check-input" id="write" name="write" value="1"> Write
                </label>
                <label class="form-check-label col-lg-2 col-md-3" for="delete">
                    <input type="checkbox" class="form-check-input" id="delete" name="delete" value="1"> Delete
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
@endsection