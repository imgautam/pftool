@extends('admin.layout')

@section('content')
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <h3 class="content-title">Create Role</h3>
            </div>
            <div class="col-lg-4 col-md-4">
                <a href="/roles" class="pull-right">View Roles</a>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <form action="/roles" method="post">
            @csrf()
            <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control" id="title" name="title" placeholder="Enter Title" required>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
@endsection