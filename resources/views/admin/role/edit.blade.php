@extends('admin.layout')

@section('content')
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <h3 class="content-title">Edit Role</h3>
            </div>
            <div class="col-lg-4 col-md-4">
                <a href="/roles" class="pull-right">View Roles</a>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <form action="/roles/{{$role->id}}" method="post">
            @method('PUT')
            @csrf()
            <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control" id="title" name="title" value="{{$role->title}}" placeholder="Enter Title" require>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection