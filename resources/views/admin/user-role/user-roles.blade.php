@extends('admin.layout')

@section('content')
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <h3 class="content-title">User Roles</h3>
            </div>
            @if('admin' == $user->type || $currentPermissions['User Role']['write'])
                <div class="col-lg-4 col-md-4">
                    <a href="user-roles/create" class="pull-right">Add New</a>
                </div>
            @endif
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">User</th>
                    <th scope="col">Roles</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                @foreach($users as $user)
                    <tr>
                        <th scope="row">{{$i++}}</th>
                        <td>{{$user->email}}</td>
                        <td>
                            @foreach($user->userRoles as $userRole) 
                                {{$userRole->role->title}}<br>
                            @endforeach
                        </td>
                        <td> 
                            @if('admin' == $user->type || $currentPermissions['User Role']['write'])   
                                <a href="/user-roles/{{$user->id}}/edit">Edit</a>
                            @endif
                                @if('admin' == $user->type || $currentPermissions['User Role']['delete'])    
                                <form action="/user-roles/{{$user->id}}" method="post">
                                    @method('DELETE')
                                    @csrf()
                                    <button type="submit" class="btn btn-link" style="color:red">Remove</button>
                                </form>
                            @endif
                        </td>
                    </tr>            
                @endforeach
            </tbody>
        </table>
    </div>
@endsection