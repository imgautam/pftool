@extends('admin.layout')

@section('content')
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <h3 class="content-title">Create User Role</h3>
            </div>
            <div class="col-lg-4 col-md-4">
                <a href="/user-roles" class="pull-right">View User Roles</a>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <form action="/user-roles" method="post">
            @csrf()
            <div class="form-group">
                <label for="roleId">Select User</label>
                <select class="form-control" id="userId" name="user_id" required>
                    <option value="" hidden>Select User</option>
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->email}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="roleId">Select Role</label>
                <select class="form-control" multiple id="roleId" name="role_ids[]" required>
                    <option value="" hidden>Select Role</option>
                    @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->title}}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
@endsection