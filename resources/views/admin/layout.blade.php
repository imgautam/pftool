<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/public/css/admin.css">
    @yield('css')
</head>
<body>
    <div class="container-fluid">
        <div class="row page">
            <div class="col-lg-12 col-md-12 top-bar">
                <a href="#">Home</a>
            </div>
            <div class="col-lg-2 col-md-2 left-menu-bar">
                <ul class="menu">
                    <li class="menu-item">
                        <a class="menu-link active" href="#">Dashboard</a>
                    </li>
                    @if('admin' == $user->type || $currentPermissions['User']['read'])
                        <li class="menu-item">
                            <a class="menu-link" href="/users">Users</a>
                        </li>
                    @endif
                    @if('admin' == $user->type || $currentPermissions['Role']['read'])
                        <li class="menu-item">
                            <a class="menu-link" href="/roles">Roles</a>
                        </li>                    
                    @endif
                    @if('admin' == $user->type || $currentPermissions['Permission']['read'])
                        <li class="menu-item">
                            <a class="menu-link" href="/permissions">Permissions</a>
                        </li>
                    @endif
                    @if('admin' == $user->type || $currentPermissions['User Role']['read'])
                        <li class="menu-item">
                            <a class="menu-link" href="/user-roles">User Roles</a>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="col-lg-10 col-md-10 main-content">
                @yield('content');
            </div>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@yield('js')
</html>
