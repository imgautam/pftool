------------------------------
PROJECT SETUP
------------------------------

1. Extract or clone the applicatoin and run command "composer install"
2. Confugure server to run applicatoin on "localhost:9090"
3. Database file is supplied with the application however to setup fresh tables run command "php artisan migrate". Database contains two users entry already:
	a) gautamtyagi.in@gmail.com - Admin who has all the rights (Password: Password24*7).
	b) mailtogautamtyagi@gmail.com - Normal User (Password: Pa$$word).
4. Change .env file and update database constants accordingly

------------------------------
HOW TO USE
------------------------------

1. Run "localhost:9090"
2. Click on "Authenticate" Button
3. If authenticated it will redirect to admin panel
4. There are 4 categories
	a) Users, b) Roles, c) Permissions, d) Role Permissions
5. On click on any category you will be navigated to that cateogry display page to show all entries in database
6. On display page there is "Add New" button on top right of page. Click on this and you will be navigated to create form where new entry can be added.
7. On display page there is "Edit" and "Remove" buttons in the last column of each row. Edit button will take you on edit page of that entry and remove button will remove that entry from database.
8. To check access management, create some roles and add permissions to roles in "Permissions" category.
9. Assign roles to user in "User Roles" category.
10. Login with some user other than admin and check that user can access only those page which are assigned to the role assigned to him.

Ex. If a user has a role which has only read permission of "Roles" category then "Add New", "Edit" and "Remove" buttons will not be available for him. Also if he tried to access it with direct url then he will be naviated to home page.

------------------------------
POINTS TO IMPROVE APPLICATION
------------------------------

1. I did not manage local auth session so we can not logout locally, however with the expire value received with jwt we can manage local auth session also. I focused on access management.
2. The application is basic we can improve it's user experience with javascript on front end or some javascript framework like Angular.
2. Backend code can also be optimized with a service layer, validations and error and exception management.